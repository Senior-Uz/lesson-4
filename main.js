// Assignment 4
//// First task

str = "Assalomu Alaykum, Hello";
let UPPER = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
let LOWER = "abcdefghijklmnopqrstuvwxyz";
let result = [];

for (let x = 0; x < str.length; x++) {
  if (UPPER.indexOf(str[x]) !== -1) {
    result.push(str[x].toLowerCase());
  } else if (LOWER.indexOf(str[x]) != -1) {
    result.push(str[x].toUpperCase());
  } else {
    result.push(str[x]);
  }
}
console.log(result.join(""));

// //// Third task
checkDuplicate();

function checkDuplicate() {
  let arr = ["abc", "xy", "bb", "abc"];
  let result = false;
  const s = new Set(arr);
  if (arr.length !== s.size) {
    result = true;
  }
  if (result) {
    console.log("Array has  duplicate elements");
  } else {
    console.log("Array has NOT duplicate elements");
  }
}

//4 /Write a JavaScript program to compute the union of two arrays.
let first = [1, 2, 3, 4, 5];
let second = [1, a, f, 3, 4, 5];
unionHandler(first, second);

function unionHandler() {
  let union = [...new Set([...first, ...second])];
  return union;
}

// 6 Write a JavaScript function that returns a passed string with letters in alphabetical order

function orderedAlph(str) {
  return str.split("").sort().join("");
}
console.log(orderedAlph("webmaster"));

//////////////////////////////////
////////////////////////////

// Task 222222222

document.querySelector("#push").onclick = function () {
  if (document.querySelector("#newtask input").value.length == 0) {
    alert("Please Enter a Task");
  } else {
    document.querySelector("#tasks").innerHTML += `
          <div class="task">
              <span id="taskname">
                  ${document.querySelector("#newtask input").value}
              </span>
              <button class="delete">
                  <i class="far fa-trash-alt"></i>
              </button>
          </div>
      `;

    var current_tasks = document.querySelectorAll(".delete");
    for (var i = 0; i < current_tasks.length; i++) {
      current_tasks[i].onclick = function () {
        this.parentNode.remove();
      };
    }

    var tasks = document.querySelectorAll(".task");
    for (var i = 0; i < tasks.length; i++) {
      tasks[i].onclick = function () {
        this.classList.toggle("completed");
      };
    }

    document.querySelector("#newtask input").value = "";
  }
};
